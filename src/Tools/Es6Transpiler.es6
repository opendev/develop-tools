

var Fs= core.System.IO.Fs
import Path from 'path'
import Child from 'child_process'
var D= core.org.voxsoftware.Developing

class Es6Transpiler{

	constructor(){
		this.funcs=[]
		this.globalCode=[]
		this.zi= 0
	}
	__procesar(src, prefix, prefixFile){

		var funcs= this.funcs
		var globalCode= this.globalCode
		//var zi=this.zi++
		this.zi++
		var code= [], ocode
		var uid
		var className=  prefix + "Namespace"
		code.push("class " + className + "{")

		if(!prefixFile)
			prefixFile= ''

		var npath, bname, y, file, ufile, files= Fs.sync.readdir(src)
		for(var i=0;i<files.length;i++){
			file= files[i]
			ufile= Path.join(src, file)
			stat= Fs.sync.stat(ufile)
			bname= file
			y= bname.indexOf(".")
			if(y>=0)
				bname= bname.substring(0, y)

			if(file[0]!="." && file[0]==file[0].toUpperCase()){
				if(stat.isDirectory()){
					this.zi++
					uid = Date.now().toString(36)+this.zi

					ocode= this.__procesar(ufile, bname, prefixFile?prefixFile+"/"+ file: file)
					funcs.push(`var ${uid}= ()=>{`)
					funcs.push(ocode)
					funcs.push("}")


					code.push("static get " + bname + "(){")
					code.push(`	return ${uid}._default || (${uid}._default= ${uid}())`)
					code.push("}")
				}
				else{

					npath= bname
					if(prefixFile)
						npath= Path.join(prefixFile, bname)

					code.push("static get " + bname + "(){")
					code.push("var e")
					code.push(`	return (e=require('./${npath}')).default ? e.default: e`)
					code.push("}")

				}
			}
		}

		code.push("}")
		if(prefix=="default")
			code.push("module.exports= "  + className)
		else
			code.push("return " + className)
		return code


	}

	procesar(src, dest){
		this.zi=0
		var funcs= this.funcs
		var code= this.__procesar(src, "default")
		var str= this.transformarCodigo(funcs)
		str+= this.transformarCodigo(code)

		var time= Date.now()
		Fs.sync.writeFile(Path.join(src, "all-namespaces.es6"), str)
		core.VW.Transpile(src, dest)
		vw.info("Tiempo transpilación: ", Date.now()-time)
		this.async= false
		this.lastSrc= src
		this.lastDest= dest
	}



	async __obtenerArchivos(src, prefix, options){
		var files= await Fs.async.readdir(src), file, stat, ufile, name
		for(var i=0;i<files.length;i++){
			file= files[i]

			if(file[0]!= "." ){

				// Tener en cuenta para convertir ...
				ufile= Path.join(src, file)
				stat= await Fs.async.stat(ufile)
				name= file
				if(name.endsWith(".es6"))
					name= Path.basename(file, ".es6")+".js"

				if(stat.isDirectory()){
					// Procesar como un directorio ...
					if(file[0]==file[0].toUpperCase())
						await this.__obtenerArchivos(ufile, Path.join(prefix, file), options)
				}
				else if(file.endsWith(".es6")||file.endsWith(".js")){

					// Añadir a los archivos totales
					options.archivosTotales[ufile]= Path.join(options.dest, prefix, name)
				}
			}
		}

		return options.archivosTotales
	}

	obtenerListaArchivos(src, dest){
		var archivosTotales= {}
		return this.__obtenerArchivos(src, '', {
			archivosTotales,
			dest
		})
	}



	dividirEnGrupos(archivosTotales){

		var grupos=[]
		// CUantos grupos?
		var keys= Object.keys(archivosTotales)
		var cantidadTotal= keys.length, cant
		if(cantidadTotal<8){
			cant= 1
		}
		else if(cantidadTotal<12){
			cant= 2
		}
		else{
			cant= Math.min(7, parseInt(cantidadTotal/3))
		}


		for(var i=0;i<cant;i++){
			grupos[i]={}
		}
		var z=0,key
		while(key=keys.shift()){
			grupos[z][key]= archivosTotales[key]
			z++
			if(z>=cant)
				z=0
		}

		return grupos

	}



	async __compile(grupo, ipc, p){
		var shm= new core.VW.IPC.ShareMethods(ipc), er
		await shm.callAsync("initialize", [])
		try{
			for(var id in grupo){

				// ReadFile 1 ....
				await shm.callAsync("Transpiler.transpile", [id, grupo[id]])
			}
		}
		catch(e){
			er=e
		}
		shm.stop()
		p.kill()

		if(er)
			throw er

	}


	waitMany(tasks){
		var count= 0
		var length= tasks.length
		var task= new core.VW.Task()
		if(length==0)
			return task.finish()

		task.result=[]
		var g= function(result){
			task.result.push(result)
			count++
			if(count==length)
				task.finish()
		}
		var y= function(err){
			task.exception= err
			count= length+1
			task.finish()
		}
		for(var i=0;i<length;i++){
			tasks[i].then(g,y)
		}
		return task
	}

	async compileAsync(grupos){
		var grupo, tasks= [], time= Date.now()
		//vw.info(grupos)
		for(var i=0;i<grupos.length;i++){
			var ipc= new core.VW.IPC.Comunication()
			var child= Child.spawn(process.argv[0],
				[
					process.argv[1],
					Path.join(__dirname, "..", "..", "ipc.js")
				]
			)
			ipc.init(child)
			grupo= grupos[i]
			tasks.push(this.__compile(grupo, ipc, child))
		}

		// Esperar las tareas ...
		//vw.info(tasks)
		await this.waitMany(tasks)
		vw.info("Tiempo transpilación: ", Date.now()-time)
	}


	async __procesarAsync(src, dest){
		this.lastSrc= src
		this.lastDest= dest
		this.zi=0
		var funcs= this.funcs
		var code= this.__procesar(src,"default")
		var str= this.transformarCodigo(funcs)
		str+= this.transformarCodigo(code)
		await Fs.sync.writeFile(Path.join(src, "all-namespaces.es6"), str)

		// Obtener la cantidad de archivos totales a transpilar
		var archivosTotales= await this.obtenerListaArchivos(src, dest)
		this.archivosTotales= archivosTotales
		var grupos= this.dividirEnGrupos(archivosTotales)

		// Ahora pues empezar a compilar todo ...
		await  this.compileAsync(grupos)

	}

	async procesarAsync(src, dest){
		this.compileTask= new core.VW.Task()
		try{
			this.compileTask.result= await this.__procesarAsync(src,dest)
		}
		catch(e){
			this.compileTask.exception=e
			//vw.error(e)


		}
		this.compileTask.finish()
	}


	getCompileConfigAsync(options={}){
		options.input= options.input || "index.es6"

		D.Tools.Es6Loader.currentTranspiler= this
		var data = {
			node: {
			    "fs": "empty"
			},
			module: {
		    	loaders: [
		    		{ test: /\.json$/, loader: "json-loader" },
		    		{ test: /\.es6$/, loader: Path.join(__dirname, "es6-loader.es6")+ "?name=[name].[ext]"}
		    	]
		  	},
		    entry:  Path.join(this.lastSrc, options.input),
		    output: {
		        path: options.output || this.lastDest,//  __dirname + "/dist",
		        filename: options.name,
		        libraryTarget: "umd"
		    }
		}
		return data
	}


	getCompileConfig(options={}){
		options.input= options.input || "index.js"
		var data = {
			node: {
			    "fs": "empty"
			},
			module: {
		    	loaders: [
		    		{ test: /\.json$/, loader: "json-loader" }
		    	]
		  	},
		    entry:  Path.join(this.lastDest, options.input),
		    output: {
		        path: options.output||this.lastDest,//  __dirname + "/dist",
		        filename: options.name,
		        libraryTarget: "umd"
		    }
		}
		return data
	}







	transformarCodigo(code){
		var c,str=''
		for(var i=0;i<code.length;i++){
			c= code[i]
			if(c instanceof Array){
				str+= '\n\n'+ this.transformarCodigo(c) + '\n\n'
			}
			else {
				str+= '\n' + c
			}
		}

		return str
	}

}



export default Es6Transpiler
