
import Path from 'path'
class Loader{

	static async process(src){

		var callback= this.async(), str, f, context, memoryStream

		
		Loader.setMain(this.resource)
		try{
			if(!Loader.main)
				throw new core.System.Exception("Debe establecer el archivo principal llamando desde el archivo de configuración")

			//memoryStream= new core.System.IO.MemoryStream()
			str=''
			context= new core.VW.E6Html.Context({
				write:function(str1){
					str+=str1
				}
			})

			//vw.info(Loader.env)
			if(Loader.env){

				for(var id in Loader.env){

					context[id]= Loader.env[id]
				}
			}
			if(Loader.es6Folder){
				await Loader.es6Folder.invoke(Loader.mainName, context)
			}
			else{
				f= new core.VW.E6Html.E6Html(Loader.main)
				await f.invoke(context)
			}

			str= "module.exports= " + JSON.stringify(str)
			callback(null, str)
		}
		catch(e){
			callback(e)
		}

	}


	static setMain(file){
		Loader.main=file
		Loader.mainName= Path.basename(file, ".es6.html")


		if(!Loader.internal_resource){
			Loader.folder= Path.dirname(file)
			Loader.es6Folder= new core.VW.E6Html.E6HtmlFolder(Loader.folder)
		}
		else{
			Loader.mainName=  Path.join( Path.relative(Loader.folder, Path.dirname(file)), Loader.mainName)
		}
	}

	static setFolder(folder){
		Loader.folder= folder
		Loader.es6Folder= new core.VW.E6Html.E6HtmlFolder(Loader.folder)
	}

}

Loader.process.loader= Loader
export default Loader
