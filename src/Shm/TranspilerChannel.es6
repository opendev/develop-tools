
class TranspilerChannel{

	static create(){

		var ipc= new core.VW.IPC.Comunication()
		ipc.init()
		var shm= new core.VW.IPC.ShareMethods(ipc)
		shm.addMethod(	function(){
			shm.addObject(new TranspilerChannel(), "Transpiler")
		},"initialize")
		return shm

	}

	transpile(src, dest){
		return core.VW.Transpile(src, dest)
	}
	
}
export default TranspilerChannel