
import Jimp from 'jimp'
import Path from 'path'
var D= core.org.voxsoftware.Developing
var Fs= core.System.IO.Fs

class Image extends D.Configuration{


	get path(){
		return this.get("path")
	}

	set path(path){
		return this.set("path", path)
	}


	get width(){
		return this.get("size.width")|0
	}

	get height(){
		return this.get("size.height")|0
	}

	get size(){
		return this.get("size")
	}

	set size(v){
		return this.set("size",v)
	}


	get fullpath(){
		var root= this.root
		var path= this.path

		if(root && root!= this){
			path= Path.join(root.directoryName, path)
		}

		return path
	}


	async getBufferAsync(){
		return Fs.async.readFile(this.fullpath)
	}

	async getBuffer(){
		return Fs.sync.readFile(this.fullpath)
	}



	async __getBufferFromJimp(image, mime){
		var task= new core.VW.Task()
		image.getBuffer(mime, function(er, data){
			if(er)
				task.exception= er
			else
				task.result= data

			task.finish()
		})
		return task
	}

	// POR IMPLEMENTAR ...
	async convertToSize(size, {keepAspect}){

		if(size.width== this.width && size.height== this.height)
			return await Fs.async.readFile(this.fullpath)



		var image= await Jimp.read(await this.getBufferAsync())
        var processed, factorx, factory, factor
        if(keepAspect){
	        while(image.bitmap.width<size.width || image.bitmap.height<size.height){
	            processed= true
	            if(image.bitmap.width<size.width){
	                image.resize(size.width, Math.round((image.bitmap.height/image.bitmap.width)*size.width))
	            }
	            else if(image.bitmap.height<size.height){
	                image.resize( (Math.round((image.bitmap.width/image.bitmap.height)*size.height)), size.height)
	            }
	        }

	        if(!processed){
	            if(image.bitmap.width>size.width && image.bitmap.height>size.height){
	                factorx= image.bitmap.width/size.width
	                factory= image.bitmap.height/size.height
	                factor=Math.min(factorx, factory)
	                image.resize(image.bitmap.width/factor, image.bitmap.height/factor)
	            }
	        }

	        image.crop((image.bitmap.width-size.width)/2, (image.bitmap.height-size.height)/2, size.width, size.height)

	    }
	    else{
	    	image.resize(size.width, size.height)
	    }

	    return await this.__getBufferFromJimp(image, Jimp.MIME_PNG)

	}

	get isLandscape(){
		return this.width>this.height
	}

	get isPortrait(){
		return !this.isLandscape
	}



}

export default Image
